﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kolokvij01 {
    sealed class Predmet {

        //Konstruktor
        public Predmet(string naziv, string odrediste, DateTime isporudenostDo, int tezina, bool opasneStvari, bool zapaljiveStvari) {
            this.Naziv = naziv;
            this.Odrediste = odrediste;
            this.IsporucenostDo = isporudenostDo;
            this.Tezina = tezina;
            if (opasneStvari && zapaljiveStvari) {
                throw new Exception("Predmet ne moze sadrzavati i opasne i zapaljive stvari!");
            }
            else {
                this.OpasneStvari = opasneStvari;
                this.ZapaljiveStvari = zapaljiveStvari;
            }
        }

        //Metode
        public override string ToString() {
            string zapaljivost;
            if (ZapaljiveStvari) {
                zapaljivost = "zapaljivo";
            }
            else {
                zapaljivost = "nije zapaljivo";
            }
            return string.Format("{0} / {1} / {2}", Naziv, Odrediste, zapaljivost);
        }

        //Getteri i setteri
        public string Naziv { get; set; }

        public string Odrediste { get; set; }

        public DateTime IsporucenostDo { get; set; }

        public int Tezina { get; set; }

        public bool OpasneStvari { get; set; }

        public bool ZapaljiveStvari { get; set; }
    }
}
