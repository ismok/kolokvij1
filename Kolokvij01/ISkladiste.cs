﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kolokvij01 {
    interface ISkladiste {
        void DodajPredmet(Predmet predmet);

        void PrikaziPredmete();
    }
}
