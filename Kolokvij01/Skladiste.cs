﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kolokvij01 {
    abstract class Skladiste {

        //Konstruktori
        public Skladiste(int maxBrojPredmeta, bool opremaZaZapaljiveStvari) {
            this.MaxBrojPredmeta = maxBrojPredmeta;
            this.OpremaZaZapaljiveStvari = opremaZaZapaljiveStvari;
            this.predmeti = new List<Predmet>();
        }

        //Metoda
        public abstract void DodajPredmet(Predmet predmet);

        public virtual void PrikaziPredmete() {
            foreach (Predmet predmet in predmeti) {
                Console.WriteLine(predmet.ToString());
            }
        }


        //Getteri i setteri
        protected int MaxBrojPredmeta {
            get {
                return MaxBrojPredmeta;
            }
            private set {
                if (value > MaxBrojPredmeta) {
                    MaxBrojPredmeta = value;
                }
                else {
                    throw new Exception("Zabranjeno smanjenje maksimalnog broja predmeta.");
                }
            }
        }

        protected bool OpremaZaZapaljiveStvari { get; set; }

        //Svojstva
        protected List<Predmet> predmeti;
    }
}
