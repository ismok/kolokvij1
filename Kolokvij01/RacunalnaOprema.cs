﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kolokvij01 {
    class RacunalnaOprema : Skladiste, ISkladiste {

        //Konstruktori
        public RacunalnaOprema(int maxBrojPredmeta, bool opremaZaZapaljiveStvari) : base(maxBrojPredmeta, opremaZaZapaljiveStvari) {
            if (maxBrojPredmeta < 500) {
                throw new Exception("Skladiste racunalneo preme mora moći sadrzavati barem 500 predmeta");
            }
      
        }

        //Metode
        public override void DodajPredmet(Predmet predmet) {
            predmeti.Add(predmet);
        }

        public override void PrikaziPredmete() {
            foreach (Predmet predmet in predmeti) {
                if(predmet.IsporucenostDo < DateTime.Now) {
                    Console.WriteLine(predmet.ToString());
                }
            }
        }

    }
}
