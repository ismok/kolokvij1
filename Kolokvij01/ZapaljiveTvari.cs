﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kolokvij01 {
    class ZapaljiveTvari : Skladiste, ISkladiste {

        //Konstruktori
        public ZapaljiveTvari(int maxBrojPredmeta) : base(maxBrojPredmeta, true) {
        }

        //Metode
        public override void DodajPredmet(Predmet predmet) {
            if (predmet.ZapaljiveStvari == true) {
                predmeti.Add(predmet);
            }
            else {
                throw new Exception("Predmet mora biti zapaljiv kako bi ga dodali u skladiste.");
            }
        }

    }
}
